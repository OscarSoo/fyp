
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Bootstrap Core CSS -->
    <link href="view/css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="view/css/modern-business.css" rel="stylesheet">
    <link href="view/css/search.css" rel="stylesheet">
    <link href="view/css/navcart.css" rel="stylesheet">
    <link href="view/css/login.css" rel="stylesheet">
    

    <!-- Custom Fonts -->
    <link href="view/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">THE 書 BookStore</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="#search" class="glyphicon glyphicon-search"></a>
                    </li>
                    <li>
                        <a href="index.php?shop">Home</a>
                    </li>
                    <li>
                        <a href="index.php?about">About Us</a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Categories<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="index.php?cat=Business">Business & Finance</a>
                            </li>
                            <li>
                                <a href="index.php?cat=Architecture">Architecture & Design</a>
                            </li>
                            <li>
                                <a href="index.php?cat='Computing">Computing & IT</a>
                            </li>
                            <li>
                                <a href="index.php?cat='Film">Film, TV & Drama</a>
                            </li>
                            <li>
                                <a href="index.php?cat=Art">Art, Fashion & Photography</a>
                            </li>
                        </ul>
                    </li>                   
                    <li>
                        <a href="index.php?contact">Contact</a>
                    </li>
<!--            login-->
                    <li class="dropdown">
                        <a href="" class=" glyphicon glyphicon-user dropdown-toggle" data-toggle="dropdown" style="font-size:17px"></a>
                        <ul class="dropdown-menu dropdown-lr" role="menu">
                            <div class="col-lg-12">
                                <div class="text-center"><h3><b>Log In</b></h3></div>
                                <form id="ajax-login-form" action="index.php?Login" method="post" role="form" autocomplete="off">
                                    <div class="form-group">
                                        <label for="username">Username</label>
                                        <input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Username" value="" autocomplete="off">
                                    </div>

                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password" autocomplete="off">
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-7">
                                                <input type="checkbox" tabindex="3" name="remember" id="remember">
                                                <label for="remember"> Remember Me</label>
                                            </div>
                                            <div class="col-xs-5 pull-right">
                                                <input type="submit" tabindex="4" class="form-control btn btn-success" name="LogIn" value="LogIn">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="text-center">
                                                    <a href="index.php?forgot" tabindex="5" class="forgot-password">Forgot Password?</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </ul>
                    </li>
                <!--./login-->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-dollar" style="font-size:19px"></i></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="index.php?curr=MYR&viewCart">MYR</a>
                            </li>
                            <li>
                                <a href="index.php?curr=GBP&viewCart">GBP</a>   
                            </li>
                            <li>
                                <a href="index.php?curr=EUR&viewCart">EUR</a>
                            </li>
                            <li>
                                <a href="index.php?curr=USD&viewCart">USD</a>
                            </li>
                        </ul>
                    </li>
                    
                            <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> <span class="glyphicon glyphicon-shopping-cart" style="font-size:17px"></span>&nbsp; <?php echo $items_in_cart ?></a>
          <ul class="dropdown-menu dropdown-cart" role="menu">
        <?php  
        if (count($_SESSION['cart']) == 0){
          echo "          <div class='basket' align='center'>
            <img src='images/cart/EmptyCart.png' height='150px'/>
            <span class='item-right'>
            </span>
        </div>" ;
        }
              foreach ($viewitems as $data)
              {
                    $string =(string)$data['Title'];
                    echo '<li>
                      <span class="item">
                        <span class="item-left">
                            <img src="'.$data['Image_url'].'" alt="" height="75" width="50"/>
                            <span class="item-info">
                                 <a href="index.php?id='.$data['ID'].'"><span>'.substr($string,0, 20).'...</span></a>
                                <span>'.$_SESSION['curr']." ".$data['Price'].'</span>
                            </span>
                        </span>
                        <span class="item-right">
                        <form action="" method="post">
                            <input type="hidden" name="key" value="'.$data['ID'].'">
                            <button class="btn btn-xs btn-danger" type="submit" name="X" value="x"/><span class="glyphicon glyphicon-remove"></span> Remove
                        </span>
                        </form> 
                        </span>
                        </form> 
                    </span>
                  </li>';
              } ?>
              <li class="divider"></li>
              <li><a class="text-center" href="index.php?viewCart">View Cart</a></li>
          </ul>
        </li>
      </ul>

                </ul>
            </div>
            
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    
    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-md-6">
                <h2 class="page-header">
                    Log In</h2>  
            </div>
            
            <div class="col-md-6">
                <h2 class="page-header">
                    New Customer?</h2>  
            </div>
        </div>
        <!-- /.row -->

        <!-- Item Row -->
        <div class="row">
            
        <div class="col-md-6">
                <div class="col-lg-12">
                                <div class="text-center"><h3><b></b></h3></div>
                                <form id="ajax-login-form" action="index.php?Login" method="post" role="form" autocomplete="off">
                                    <div class="form-group">
                                        <label for="username">Username</label>
                                        <input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Username" value="" autocomplete="off">
                                    </div>

                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password" autocomplete="off">
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-7">
                                                <input type="checkbox" tabindex="3" name="remember" id="remember">
                                                <label for="remember"> Remember Me</label>
                                            </div>
                                            <div class="col-xs-5 pull-right">
                                                <input type="submit" tabindex="4" class="form-control btn btn-success" name="LogIn" value="LogIn">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="text-center">
                                                   <a href="index.php?forgot" tabindex="5" class="forgot-password">Forgot Password?</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>    
        </div>

        <div class="col-md-6">
            <div class="text-center"><h3><b></b></h3></div>
								<form id="ajax-register-form" action="index.php?Login" method="post" role="form" autocomplete="off">
									<div class="form-group">
                                        <label for="username">Creat an Account:</label>
										<input type="text" name="Rusername" id="username" tabindex="1" class="form-control" placeholder="Username" value="">
									</div>
									<div class="form-group">
										<input type="email" name="Remail" id="email" tabindex="1" class="form-control" placeholder="Email Address" value="">
									</div>
									<div class="form-group">
										<input type="password" name="Rpassword" id="password" tabindex="2" class="form-control" placeholder="Password">
									</div>
									<div class="form-group">
										<input type="password" name="confirm-password" id="confirm-password" tabindex="2" class="form-control" placeholder="Confirm Password">
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-xs-6 col-xs-offset-3">
												<input type="submit" name="LogIn" id="register-submit" tabindex="4" class="form-control btn btn-info" value="LogIn">
											</div>
										</div>
									</div>
								</form>
                            </div>
        </div>
                
        </div>
        <!-- /.row -->


        <hr>

                <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12"> 
                    <p>Copyright &copy; Bookstore 2017</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- ./Page Content -->
    
                <!--    Search Full Screen-->
    <div id="search">
    <button type="button" class="close">×</button>
    <form>
        <input type="search" value="" placeholder="type keyword(s) here" />
        <a href = "index.php?search" class="btn btn-primary">Search</a>
    </form>
    </div>
    <!--    ./Search Full Screen-->

    <!-- jQuery -->
    <script src="view/js/jquery.js"></script>
    <script src="view/js/search.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="view/js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>

</body>

</html>
