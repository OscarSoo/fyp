<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Modern Business - Start Bootstrap Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="view/css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="view/css/modern-business.css" rel="stylesheet"> 
    <link href="view/css/search.css" rel="stylesheet">
    <link href="view/css/relatedbook.css" rel="stylesheet">
    <link href="view/css/imghover.css" rel="stylesheet">
    <link href="view/css/navcart.css" rel="stylesheet">
    <link href="view/css/login.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="view/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
            <?php 

            foreach ($selectedBook as $data)
            {
                    $ID = $data['ID'];
                    $Title = $data['Title'];
                    $Description = $data['Description'];
                    $Image_url = $data['Image_url'];
                    $Category = $data['Category'];
                    $Price = $data['Price'];
                    $Author = $data['Author'];
            } ?> 

</head>

<body>

        <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">THE 書 BookStore</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="#search" class="glyphicon glyphicon-search"></a>
                    </li>
                    <li>
                        <a href="index.php?shop">Home</a>
                    </li>
                    <li>
                        <a href="index.php?about">About Us</a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Categories<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="index.php?cat=Business">Business & Finance</a>
                            </li>
                            <li>
                                <a href="index.php?cat=Architecture">Architecture & Design</a>
                            </li>
                            <li>
                                <a href="index.php?cat='Computing">Computing & IT</a>
                            </li>
                            <li>
                                <a href="index.php?cat='Film">Film, TV & Drama</a>
                            </li>
                            <li>
                                <a href="index.php?cat=Art">Art, Fashion & Photography</a>
                            </li>
                        </ul>
                    </li>                   
                    <li>
                        <a href="index.php?contact">Contact</a>
                    </li>
<!--            login-->
                    <li class="dropdown">
                        <a href="" class=" glyphicon glyphicon-user dropdown-toggle" data-toggle="dropdown" style="font-size:17px"></a>
                        <ul class="dropdown-menu dropdown-lr" role="menu">
                                                   <?php 
                            if (!isset($_SESSION['login_user_ID']))
                            {
                            echo '
                            <div class="col-lg-12">
                                <div class="text-center"><h3><b>Log In</b></h3></div>
                                <form id="ajax-login-form" action="index.php?Login" method="post" role="form" autocomplete="off">
                                    <div class="form-group">
                                        <label for="username">Username</label>
                                        <input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Username" value="" autocomplete="off">
                                    </div>

                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password" autocomplete="off">
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-7">
                                                <input type="checkbox" tabindex="3" name="remember" id="remember">
                                                <label for="remember"> Remember Me</label>
                                            </div>
                                            <div class="col-xs-5 pull-right">
                                                <input type="submit" tabindex="4" class="form-control btn btn-success" name="LogIn" value="LogIn">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="text-center">
                                                    <a href="index.php?forgot" tabindex="5" class="forgot-password">Forgot Password?</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>'; }
                            
                            if(isset($_SESSION['login_user_ID']))
                            {
                                echo '<li>
                                    <a href="index.php?profile">View Profile</a>
                                </li>

                                <li>
                                    <a href="index.php?logout">Logout</a>
                                </li>';
                            }
                        
                            ?>
                        </ul>
                    </li>
                <!--./login-->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-dollar" style="font-size:19px"></i></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="index.php?curr=MYR&id=<?php echo $ID?>">MYR</a>
                            </li>
                            <li>
                                <a href="index.php?curr=GBP&id=<?php echo $ID?>">GBP</a>   
                            </li>
                            <li>
                                <a href="index.php?curr=EUR&id=<?php echo $ID?>">EUR</a>
                            </li>
                            <li>
                                <a href="index.php?curr=USD&id=<?php echo $ID?>">USD</a>
                            </li>
                        </ul>
                    </li>
                    
                            <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> <span class="glyphicon glyphicon-shopping-cart" style="font-size:17px"></span>&nbsp; <?php echo $items_in_cart ?></a>
          <ul class="dropdown-menu dropdown-cart" role="menu">
        <?php  
        
        if (count($_SESSION['cart']) == 0){
          echo "          <div class='basket' align='center'>
            <img src='images/cart/EmptyCart.png' height='150px'/>
            <span class='item-right'>
            </span>
        </div>" ;
        }
              foreach ($viewitems as $data)
              {
                    $string =(string)$data['Title'];
                    echo '<li>
                      <span class="item">
                        <span class="item-left">
                            <img src="'.$data['Image_url'].'" alt="" height="75" width="50"/>
                            <span class="item-info">
                                 <a href="index.php?id='.$data['ID'].'"><span>'.substr($string,0, 20).'...</span></a>
                                <span>'.$_SESSION['curr']." ".$data['Price'].'</span>
                            </span>
                        </span>
                        <span class="item-right">
                        <form action="" method="post">
                            <input type="hidden" name="key" value="'.$data['ID'].'">
                            <button class="btn btn-xs btn-danger" type="submit" name="X" value="x"/><span class="glyphicon glyphicon-remove"></span> Remove
                        </span>
                        </form> 
                        </span>
                        </form> 
                    </span>
                  </li>';
              } ?>
              <li class="divider"></li>
              <li><a class="text-center" href="index.php?viewCart">View Cart</a></li>
          </ul>
        </li>
      </ul>

                </ul>
            </div>
            
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">
                    <?php echo $Title ?><small> &nbsp; &nbsp;<?php echo $Author ?> </small></h2>  
            </div>
        </div>
        <!-- /.row -->

        <!-- Item Row -->
        <div class="row">
            
        <div class="col-md-4">
                <img class="img-responsive img-portfolio img-hover" src='<?php echo $Image_url ?>'>
            </div>

            <div class="col-md-8">
                <h3>Description</h3><p><?php echo $Description ?></p>
                <h4>Category:<small> &nbsp; &nbsp;<?php echo $Category ?></small></h4><p>
                <h3>Price: <?php
                            echo  $_SESSION['curr']." ".$Price;
                                   
                                    ?></h3><br/>
                <form action="index.php?addCart=<?php echo $ID?>&id=<?php echo $ID?>" method="POST">
                    <input type="hidden" name="id" value="<?php  $ID?>">
                    <input type="hidden" name="curr" value="<?php $curr?>">
                    <input type="hidden" name="addCart" value="<?php $ID?>">
                  <input class="btn btn-default btn-lg" type="submit" value="Add to Cart">
                                <a href="index.php?id=<?php echo $ID?>&curr=<?php echo $curr?>&addView=<?php echo $ID?>" class="btn btn-default btn-lg">Add to Viewlist</a> 
                </form>
                
<!--                <a href="index.php?id=<?php echo $ID?>&curr=<?php echo $curr?>&addCart=<?php echo $ID?>" class="btn btn-default btn-lg">Add to Cart</a> -->
                

            </div>

        </div>
        <!-- /.row -->

        <!-- Related Books Row -->
        <div class="row">
            <div class='row'>
    <div class='col-md-12'>
      <div class="carousel slide media-carousel" id="media">
        <div class="carousel-inner">
          <div class="item  active">
            <div class="row">
                

                
            <?php 
    $counter = 0;

        foreach ($books as $data)
            {
                if($counter >= 4)
                {       
                    break;
                } 
                else{
                    echo '                <div class="col-md-3"><div class="container1">
                    <a href="index.php?id='.$data['ID'].'">
  <img src="'.$data['Image_url'].'"  class=" thumbnail img-responsive img-portfolio img-hover" ></a>
  <div class="middle">
    <div class="text1">'.$_SESSION['curr']." ".$data['Price'].'</div>
  </div></div>
</div>';
                }
                $counter++;
                      
            }
                ?>
            </div>
          </div>
          <div class="item">
            <div class="row">
                
            <?php 
    $counter = 0;
        shuffle($books);
        foreach ($books as $data)
            {
                if($counter >= 4)
                {       
                    break;
                } 
                else{
                    echo '                <div class="col-md-3"><div class="container1">
                    <a href="index.php?id='.$data['ID'].'">
  <img src="'.$data['Image_url'].'"  class=" thumbnail img-responsive img-portfolio img-hover" ></a>
  <div class="middle">
    <div class="text1">'.$_SESSION['curr']." ".$data['Price'].'</div>
  </div></div>
</div>';
                }
                $counter++;
                      
            }?>
            </div>
          </div>
          <div class="item">
                    <div class="row">

            <?php 
    $counter = 0;
        shuffle($books);                
        foreach ($books as $data)
            {
                if($counter >= 4)
                {       
                    break;
                } 
                else{
                    echo '  
                    <div class="col-md-3">
                        <div class="container1">
                    <a href="index.php?id='.$data['ID'].'">
                    <img src="'.$data['Image_url'].'"  class=" thumbnail img-responsive img-portfolio img-hover" ></a>
                            <div class="middle">
                                <div class="text1">'.$_SESSION['curr']." ".$data['Price'].'</div>
                            </div>                   
                        </div>
                    </div>';
                }
                $counter++;
                      
            }?>
            </div>
          </div>
        </div>
        <a data-slide="prev" href="#media" class="left carousel-control">‹</a>
        <a data-slide="next" href="#media" class="right carousel-control">›</a>
      </div>                          
    </div>

        </div>
        <!-- /.row -->

        <hr>

                <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12"> 
                    <p>Copyright &copy; Bookstore 2017</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->
    
            <!--    Search Full Screen-->
    <div id="search">
    <button type="button" class="close">×</button>
    <form>
        <input type="search" value="" placeholder="type keyword(s) here" />
        <a href = "index.php?search" class="btn btn-primary">Search</a>
    </form>
    </div>
    <!--    ./Search Full Screen-->

        <!-- jQuery -->
    <script src="view/js/jquery.js"></script>
    <script src="view/js/search.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="view/js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>

</body>

</html>
