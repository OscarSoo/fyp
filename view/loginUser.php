
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Bootstrap Core CSS -->
    <link href="view/css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="view/css/modern-business.css" rel="stylesheet">
    <link href="view/css/search.css" rel="stylesheet">
    <link href="view/css/navcart.css" rel="stylesheet">
    <link href="view/css/login.css" rel="stylesheet">
    <link href="view/css/loginUser.css" rel="stylesheet">
    <link href="view/vendor/css/grayscale.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="view/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
    <?php
            foreach ($info as $row)
            {         
                    $email = $row["email"];
                    $address = $row["address"];
                    $city = $row["city"];
                    $postcode = $row["postcode"];
                    $telephone = $row["telephone"];
            } 
    
    ?> 

    ?>

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">THE 書 BookStore</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="#search" class="glyphicon glyphicon-search"></a>
                    </li>
                    <li>
                        <a href="index.php?shop">Home</a>
                    </li>
                    <li>
                        <a href="index.php?about">About Us</a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Categories<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="index.php?cat=Business">Business & Finance</a>
                            </li>
                            <li>
                                <a href="index.php?cat=Architecture">Architecture & Design</a>
                            </li>
                            <li>
                                <a href="index.php?cat='Computing">Computing & IT</a>
                            </li>
                            <li>
                                <a href="index.php?cat='Film">Film, TV & Drama</a>
                            </li>
                            <li>
                                <a href="index.php?cat=Art">Art, Fashion & Photography</a>
                            </li>
                        </ul>
                    </li>                   
                    <li>
                        <a href="index.php?contact">Contact</a>
                    </li>
<!--            login-->
                    <li class="dropdown">
                        <a href="" class=" glyphicon glyphicon-user dropdown-toggle" data-toggle="dropdown" style="font-size:17px"></a>
                        <ul class="dropdown-menu dropdown-lr" role="menu">
                                                   <?php 
                            if (!isset($_SESSION['login_user_ID']))
                            {
                            echo '
                            <div class="col-lg-12">
                                <div class="text-center"><h3><b>Log In</b></h3></div>
                                <form id="ajax-login-form" action="index.php?Login" method="post" role="form" autocomplete="off">
                                    <div class="form-group">
                                        <label for="username">Username</label>
                                        <input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Username" value="" autocomplete="off">
                                    </div>

                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password" autocomplete="off">
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-7">
                                                <input type="checkbox" tabindex="3" name="remember" id="remember">
                                                <label for="remember"> Remember Me</label>
                                            </div>
                                            <div class="col-xs-5 pull-right">
                                                <input type="submit" tabindex="4" class="form-control btn btn-success" name="LogIn" value="LogIn">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="text-center">
                                                    <a href="index.php?forgot" tabindex="5" class="forgot-password">Forgot Password?</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>'; }
                            
                            if(isset($_SESSION['login_user_ID']))
                            {
                                echo '<li>
                                    <a href="index.php?profile">View Profile</a>
                                </li>
                                <li>
                                    <a href="index.php?logout">Logout</a>
                                </li>';
                            }
                        
                            ?>
                        </ul>
                    </li>
                <!--./login-->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-dollar" style="font-size:19px"></i></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="index.php?curr=MYR&profile">MYR</a>
                            </li>
                            <li>
                                <a href="index.php?curr=GBP&profile">GBP</a>   
                            </li>
                            <li>
                                <a href="index.php?curr=EUR&profile">EUR</a>
                            </li>
                            <li>
                                <a href="index.php?curr=USD&profile">USD</a>
                            </li>
                        </ul>
                    </li>
                    
                            <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> <span class="glyphicon glyphicon-shopping-cart" style="font-size:17px"></span>&nbsp; <?php echo $items_in_cart ?></a>
          <ul class="dropdown-menu dropdown-cart" role="menu">
        <?php  
        if (count($_SESSION['cart']) == 0){
          echo "          <div class='basket' align='center'>
            <img src='images/cart/EmptyCart.png' height='150px'/>
            <span class='item-right'>
            </span>
        </div>" ;
        }
              foreach ($viewitems as $data)
              {
                    $string =(string)$data['Title'];
                    echo '<li>
                      <span class="item">
                        <span class="item-left">
                            <img src="'.$data['Image_url'].'" alt="" height="75" width="50"/>
                            <span class="item-info">
                                 <a href="index.php?id='.$data['ID'].'"><span>'.substr($string,0, 20).'...</span></a>
                                <span>'.$_SESSION['curr']." ".$data['Price'].'</span>
                            </span>
                        </span>
                        <span class="item-right">
                        <form action="" method="post">
                            <input type="hidden" name="key" value="'.$data['ID'].'">
                            <button class="btn btn-xs btn-danger" type="submit" name="X" value="x"/><span class="glyphicon glyphicon-remove"></span> Remove
                        </span>
                        </form> 
                        </span>
                        </form> 
                    </span>
                  </li>';
              } ?>
              <li class="divider"></li>
              <li><a class="text-center" href="index.php?viewCart">View Cart</a></li>
          </ul>
        </li>
      </ul>

            </div>
            
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    
    <!-- Page Content -->
        <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Hello, <?php echo $_SESSION['login_user']; ?>!<small>  &nbsp;  &nbsp;&nbsp;Welcome To 書 BookStore. </small></h1>  
            </div>
        </div>
                 <div class="row">
        <div class="wrapper-inner-tab" align="center"">
        <div class="wrapper-inner-tab-backgrounds-first" style="background-image: url(images/user/1.jpg)"><a class="page-scroll" href="#profile"><div class="sim-button button1"> <span class="page-scroll" href="#viewProfile">View Profile</span></div></a></div>
                     
        <div class="wrapper-inner-tab-backgrounds-second" style="background-image: url(images/user/2.jpg)"><a class="page-scroll" href="#order"><div class="sim-button button1"><span>View Order</span></div></a></div>
                                                                                                                                          
        <div class="wrapper-inner-tab-backgrounds-third" style="background-image: url(images/user/3.jpg)"><a class="page-scroll" href="#list"><div class="sim-button button1"><span>View ViewList</span></div></a></div>
</div>   </div> "
             
             
                      
             
        <section id="profile" class=" content-section text-center">
        
        <div class="aboutBG">
        <div class="row">
            <div class="col-md-6">
                 <div class="text-center"><h3><b>Your info:</b></h3></div>
									<div class="form-group">
										<input type="email" name="Uemail" id="Uemail" tabindex="1" class="form-control" placeholder="Email Address" value="<?php echo $email ?>" disabled>
									</div>
                                    <div class="form-group">                             
										<input type="text" name="Uaddress" id="Uaddress" tabindex="1" class="form-control" placeholder="Address" value="<?php echo $address ?>" disabled>
									</div>
                                    <div class="form-group">                             
										<input type="text" name="Ucity" id="Ucity" tabindex="1" class="form-control" placeholder="City" value="<?php echo $city ?>" disabled>
									</div>
                                    <div class="form-group">                             
										<input type="text" name="Upostcode" id="Upostcode" tabindex="1" class="form-control" placeholder="Postcode" value="<?php echo $postcode ?>" disabled>
									</div>
                                    <div class="form-group">                             
										<input type="text" name="Utelephone" id="Utelephone" tabindex="1" class="form-control" placeholder="Telephone" value="<?php echo $telephone ?>" disabled>
									</div>
            </div>
                    <div class="col-md-6">
                                <div class="text-center"><h3><b>Update your info:</b></h3></div>
								<form id="ajax-register-form" action="index.php?Update&Update1" method="post" role="form" autocomplete="off">
									<div class="form-group">
										<input type="email" name="Uemail" id="Uemail" tabindex="1" class="form-control" placeholder="Email Address" value="">
									</div>
                                    <div class="form-group">                             
										<input type="text" name="Uaddress" id="Uaddress" tabindex="1" class="form-control" placeholder="Address" value="">
									</div>
                                    <div class="form-group">                             
										<input type="text" name="Ucity" id="Ucity" tabindex="1" class="form-control" placeholder="City" value="">
									</div>
                                    <div class="form-group">                             
										<input type="text" name="Upostcode" id="Upostcode" tabindex="1" class="form-control" placeholder="Postcode" value="">
									</div>
                                    <div class="form-group">                             
										<input type="text" name="Utelephone" id="Utelephone" tabindex="1" class="form-control" placeholder="Telephone" value="">
									</div>
                                    <div class="form-group">
										<input type="password" name="Upassword" id="Upassword" tabindex="2" class="form-control" placeholder="Password" required>
									</div>
									<div class="form-group">
										<input type="password" name="confirm-password" id="confirm-password" tabindex="2" class="form-control" placeholder="Confirm Password" required>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-xs-6 col-xs-offset-3">
												<input type="submit" name="Update" id="register-submit" tabindex="4" class="form-control btn btn-info" value="Update" >
											</div>
										</div>
									</div>
								</form>
                            </div>
        </div>
        </div>
    </section>  
            
            <hr>
    <section id="order" class=" content-section text-center">
        
        <div class="aboutBG">
        <div class="row">
            <div class="text-center"><h3><b>Your Orders:</b></h3></div>
            <div class="col-md-12">
                    <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>Order Date</th>
                        <th>Order Number</th>
                        <th>Total Amount</th>
                        <th>Order Status</th>
                          <th></th>
                      </tr>
                    </thead>
                    <tbody>
    <?php                
            foreach ($order as $row)
            {         
                echo '<tr>
                    <td> '.$row["orderdate"].'</td>
                    <td> '.$row["ordernumber"].'</td>
                    <td> '.$row["total"].'</td>
                    <td> '.$row["status"].'</td>
                    <td><a href ="#" style="color:blue;">View</a></td>
                </tr>';
                
            } ?>


                    </tbody>
                  </table>
            </div>
               
        </div>
    </section>  
        
    <section id="list" class=" content-section text-center">
        
        <div class="row">
            <div class="text-center"><h3><b>Your ViewLists:</b></h3></div>
            <div class="col-md-12">
                    <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>Title</th>
                        <th>Author</th>
                        <th>Status</th>
                          <th></th>
                      </tr>
                    </thead>
                    <tbody>
    <?php                

            foreach ($viewlist as $row)
            {         
                echo '<tr>
                    <td>'.$row["Title"].'</td>
                    <td>'.$row["Author"].'</td>';
                
                if($row["Stock"] == 0 )
                {
                    echo '<td><font color="red">Out Of Stock</font></d>';
                }
                else
                {
                    echo '<td><font color="green">In Stock</font></d>';
                }
                    
                    
                echo '<td><a href ="index.php?id='.$row["ID"].'" style="color:blue;">View</a></td>
                </tr>';
                
            } 
                        ?>
                    </tbody>
                  </table>
               
        </div>
    </section>                   



    <!-- ./Page Content-->
    


        <hr>

                <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12"> 
                    <p>Copyright &copy; Bookstore 2017</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- ./Page Content -->
            <!--    Search Full Screen-->
    <div id="search">
    <button type="button" class="close">×</button>
    <form>
        <input type="search" value="" placeholder="type keyword(s) here" />
        <a href = "index.php?search" class="btn btn-primary">Search</a>
    </form>
    </div>
    <!--    ./Search Full Screen-->

    <!-- jQuery -->
    <script src="view/js/jquery.js"></script>
    <script src="view/js/search.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="view/js/bootstrap.min.js"></script>
    <script src="view/vendor/js/grayscale.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>

</body>

</html>