<?php
include("config.php");

class Model 
{  
    public function ListBook($set, $curr)   #listBook 
    {   
        global $conn;
        
        $query = "SELECT * from stocks";
         if (isset($_GET['shop'])){ //only show in homepage as bestseller
        $query .= " WHERE sold >= 50"; }      
        $query .= " ORDER BY RAND()";

        $result = $conn->query($query);
        $return_result = array();

        if ($result->num_rows > 0) 
            {		                    
                while($row = $result->fetch_assoc())
                    {  
                        $price = $this->Currency('GBP',$curr,$row["Price"]);
                        $return_result[] = array( 
                             'ID' => $row["ID"],
                             'Image_url' => $row["Image_url"],
                             'Price' => $price,
                            );
                    }   		
                return $return_result;
            }      
    }  
    
    public function SelectedBook($selectedBook,$curr)  //Show selected books
    {   
        global $conn;

        $result = $conn->query("SELECT * FROM stocks WHERE id ='$selectedBook'");
        $return_result = array();

        if ($result->num_rows > 0) 
            {		                    
                while($row = $result->fetch_assoc())
                    {  

                        $Price = $this->Currency('GBP',$curr,$row["Price"]);
                        //return var_dump($row);
                        $return_result[] = array( 
                                'ID' => $row["ID"],
                                'Title' => $row["Title"],
                                'Description' => $row["Description"],
                                'Price' => $Price,
                                'Image_url' => $row["Image_url"],
                                'Category' => $row["Category"],
                                'Author' => $row['Author']

                            );
                    }   		
                return $return_result;
            }              
    }  
     
    public function CategoryBook($category)  //List categories books
    {   
        global $conn;

        $result = $conn->query("SELECT * FROM stocks WHERE Category = '$category' ORDER BY RAND()");
        $return_result = array();

        if ($result->num_rows > 0) 
            {		                    
                while($row = $result->fetch_assoc())
                    {  
                        //return var_dump($row);
                        $return_result[] = array( 
                                'ID' => $row["ID"],
                                'Image_url' => $row["Image_url"],
                            );
                    }   		
                return $return_result;
 
    }
    }
  
    public function Currency($inputFrom, $inputTo, $amount)     //Currency Conversion REST
    {
        $Ratexml = simplexml_load_file(RATES) or die("Not Loading"); 

        foreach ($Ratexml->resource as $resource)
        {
                $RateCode = $resource->code;
                $RateRate = $resource->rate;
                $RateTs   = intval($resource->ts);

//               # Check timestamp if it's more than 12 hours 
//                if($RateTs  < time() - (60*60*36) && $RateCode != 'USD')
//                {
//                    $this->UpdateRates();  
//                    $update = "update";
//                    return $update;
//                } 
        } 

        foreach ($Ratexml->resource as $resource) 
        {
            $RateCode = $resource->code;

            if ($inputFrom == $RateCode) {    
                $FromArray[] = $resource;           
            }
            if ($inputTo == $RateCode){
                $ToArray[] = $resource;
            }
        }

        $FromArray = $FromArray[0];
        $FromCode = $FromArray->code;
        $FromRate = $FromArray->rate;

        $ToArray = $ToArray[0];
        $ToCode = $ToArray->code;
        $ToRate = $ToArray->rate;

        $result = $amount / floatval($FromRate) * floatval($ToRate); //calculation


        return number_format($result,2);
    }
    
    public function UpdateRates()   //Update Rates.xml if it's more than 12 hours
    {
        $Ratexml = simplexml_load_file(RATES) or die("Not Loading"); 
        $RatesURL = simplexml_load_file(RATES_URL) or die("Not Loading");

        foreach ($Ratexml->resource as $XMLresource) //Loop local xml
        {
            $XMLcode = $XMLresource->code;

            foreach ($RatesURL->resources->resource as $resource)
            {
                $code = substr($resource->field[2],0,-2);

                        $rate = strval($resource->field[1]);
                        $ts =   (string)$resource->field[3]; 

                if(strncasecmp($XMLcode,$code, 3) == 0)  
                {
                    $XMLresource->rate = $rate;
                    $XMLresource->ts = $ts;
                    $Ratexml->saveXML(RATES);
                }
            }
        }
//        return $Ratexml;
    }
    
    public function CurrencySession()   //Store Currency setting into Session
    {
            if(isset($_GET['curr'])) //if selected then show that currency
            {
                return $_SESSION['curr'] = $_GET['curr'];            
            }
            if(!isset($_SESSION['curr']))
            {
                $curr = "GBP";
                return $curr;
            }
  
    }
    
    public function CartSession() //Session array to store cart
    {               
        if(isset($_GET['addCart']) && isset($_POST['curr']) && isset($_POST['id']))
        {
            $_SESSION['cart'][] = $_GET['addCart'];  
        }
        return $_SESSION['cart'];
    }  
    
    public function DeleteItem() //Session unset an array from the cart 
    {
        if(isset($_POST['X']))
        {
//            $_POST['key'] = " ";
            $input = $this->searchForKey($_POST['key'], $_SESSION['cart']); 
            unset($_SESSION['cart'][$input]);
        }
    }
    
    public function ViewCart($curr) // View Cart
    {
        $Cart = $this->CartSession();
        global $conn;
        $return_result = array();
        if (isset($_SESSION['cart'])){
        foreach($_SESSION['cart'] as $ID)
        {
            $result = $conn->query("SELECT * FROM stocks WHERE id ='$ID'");
            
            if ($result->num_rows > 0) 
            {		                    
                while($row = $result->fetch_assoc())
                    {  

                        $Price = $this->Currency('GBP',$curr,$row["Price"]);
                        $return_result[] = array( 
                                'ID' => $row["ID"],
                                'Title' => $row["Title"],
                                'Price' => $Price,
                                'Image_url' => $row["Image_url"],
                                'Author' => $row["Author"],
                            
                            );
                    }   		
                
            }  
        }
        }
        return $return_result;
    }
    
    function searchForKey($id, $array) 
    {
       foreach ($array as $key => $val) {
           if ($val == $id) {
               return $key;
           }
       }
       return null;
}
    
    public function userLogin($username,$password)
    {
    
    global $conn;
     
    $username = $_POST['username'];
    $password = $_POST['password'];
     

            $query="SELECT * FROM user WHERE username='$username' AND password ='$password'";
            $result = $conn->query($query);

            if($result->num_rows > 0 )
                {
                    while($row = $result->fetch_assoc())
                    {  
                    $ID = $row["ID"];
                    }
                    $_SESSION['login_user']=$username;
                    $_SESSION['login_user_ID']= $ID;
                return true;
                }
            else
                {
                return false;
                }
    }
    
    public function userRegister()
    {
    
    global $conn;
     
    $username = $_POST['Rusername'];
    $password = $_POST['Rpassword'];
    $email = $_POST['Remail'];
     
            
            $query="INSERT INTO user (username, password, email)
                    VALUES ('$username','$password', '$email');";
            $result = $conn->query($query);
            if($result)
            {
                        $message = "Registration Succesful.";
                        echo "<script type='text/javascript'>alert('$message');</script>";  
            }

    }
    
    public function userInfo()
    {
    
    global $conn;
     
    $ID = $_SESSION['login_user_ID'];

            
            $query="SELECT * FROM user WHERE ID='$ID'";
            $result = $conn->query($query);

            if($result->num_rows > 0 )
                {
                    while($row = $result->fetch_assoc())
                    {  

                        $return_result[] = array( 
                            'email' => $row["email"],
                            'address' => $row["address"],
                            'city' => $row["city"],
                            'postcode' => $row["postcode"],
                            'telephone' => $row["telephone"]
                             
                            );
                    }
                return $return_result;
                }
    }
    
    public function userUpdate()
    {
    
    global $conn;
     
    $ID = $_SESSION['login_user_ID'];
    $email = $_POST['Uemail'];
    $address = $_POST['Uaddress'];
    $city = $_POST['Ucity'];
    $postcode = $_POST['Upostcode'];
    $telepone = $_POST['Utelephone'];
    $password = $_POST['Upassword'];
                    
    $query = "";
                    if (!empty($password))
					{
						$query .= " password ='$password' ";
					}
					if (!empty($address))
					{
						$query .= " ,address ='$address' ";
					}
					if (!empty($city))
					{
						$query .= " ,city ='$city' ";
					}
					if (!empty($postcode))
					{
						$query .= ",postcode ='$postcode' ";
					}
					if (!empty($telepone))
					{
						$query .= " ,telephone ='$telepone' ";
					}
					if (!empty($email))
					{
						$query .= " ,email ='$email' ";
					}
            
            $result = $conn->query("UPDATE user SET $query WHERE ID = '$ID'");
        
            if($result == true)
            {
                $message = "Updated Succesful.";
                 echo "<script type='text/javascript'>alert('$message');</script>";   
            }
    }
    
    public function userOrder()
    {
        global $conn;
        
        $ID = $_SESSION['login_user_ID'];
        
        $query="SELECT * FROM orders";
        $result = $conn->query($query);

            if($result->num_rows > 0 )
                {
                    while($row = $result->fetch_assoc())
                    {  

                        $return_result[] = array( 
                            'orderdate' => $row["orderdate"],
                            'ordernumber' => $row["ordernumber"],
                            'total' => $row["total"],
                            'status' => $row["status"],                          
                            );
                    }
                return $return_result;
                }
    }
    
    public function addView()
    {
        global $conn;
             
        if(isset($_SESSION['login_user_ID']))
        {
            $customerID = $_SESSION['login_user_ID']; 
            if(isset($_GET['addView']))
            {
                $bookID = $_GET['addView'];
                $query="INSERT INTO viewlist (customerID, bookID)
                VALUES ('$customerID','$bookID');";

                $result = $conn->query($query);
                if($result)
                {
                            $message = "Added Succesful.";
                            echo "<script type='text/javascript'>alert('$message');</script>";  
                }
            }
        }
        else
        {
            $message = "Please login first.";
            echo "<script type='text/javascript'>alert('$message');</script>";     
        }
        
    }
    
    public function contact()
    {
//          //Email information
//          $admin_email = "soozhengshen@gmail.com";
//          $email = $_REQUEST['email'];
//          $subject = $_REQUEST['subject'];
//          $comment = $_REQUEST['message'];
//          
//        //send email
//          mail($admin_email, "$subject", $comment, "From:" . $email);

        if(isset($_POST['contact']))
        {
            $message = "Thank you for contacting us. We will be in touch with you very soon.";
            echo "<script type='text/javascript'>alert('$message');</script>";    
        }
        
    }
    
    public function searchBook($book,$curr)
    {
        $book = preg_replace('/\s+/', '', $book);
        
        global $conn;

        $result = $conn->query("SELECT * FROM stocks");
        $return_result = array();

        if ($result->num_rows > 0) 
            {		                    
                while($row = $result->fetch_assoc())
                    {  

                        $Price = $this->Currency('GBP',$curr,$row["Price"]);
                        //return var_dump($row);
                        $return_result[] = array( 
                                'ID' => $row["ID"],
                                'Title' => $row["Title"],
                            );
                    }   		
            } 
        foreach ($return_result as $data)
        {
            $stock = preg_replace('/\s+/', '', $data['Title']);
            if ($book == $stock)
            {
                return $data['ID'];
            }
        }
        
        
    }
    
    public function userViewlist($curr)
    {
        global $conn;
        
        $ID = $_SESSION['login_user_ID'];
        
        $result = $conn->query("SELECT * FROM viewlist WHERE customerID = '$ID'");
        
        $return_result = array();

        if ($result->num_rows > 0) 
            {		                    
                while($row = $result->fetch_assoc())
                    {  
                        $return_result[] = array( 
                                'ID' => $row["bookID"]
                            );
                    }   		
            } 
        foreach ($return_result as $data)
        {
            $bookID = $data['ID'];
            $result = $conn->query("SELECT * FROM stocks WHERE ID = '$bookID'");
            
            if ($result->num_rows > 0) 
            {		                    
                while($row = $result->fetch_assoc())
                    {  

                        $Price = $this->Currency('GBP',$curr,$row["Price"]);
                        $viewlist[] = array( 
                                'ID' => $row["ID"],
                                'Title' => $row["Title"],
                                'Price' => $Price,
                                'Stock' => $row["Stocks"],
                                'Author' => $row["Author"],
                            );
                    }   		
            } 
            
        }
         return $viewlist;
    }
    
    

    
}  //model class close

?>