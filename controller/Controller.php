<?php

//Controller
include_once("model/Model.php");  
  
class Controller {  
     
    public $model; 
  
     public function __construct()    
     {    
          $this->model = new Model();  
     }   
      
     public function display()  
     {             
            $curr = $this->model->CurrencySession();        
            $this->model->DeleteItem();
            $viewitems = $this->model->ViewCart($_SESSION['curr']);          
            $items_in_cart = is_array($_SESSION['cart']) ? count($_SESSION['cart']) : 0;
            
            if(isset($_GET['Update']))
            {
                $this->model->userUpdate(); 
            }
            
            //Page direct
            if (isset($_GET['shop'])){ 
                $books = $this->model->ListBook($_GET['shop'],$_SESSION['curr']); 
                include 'view/home.php';  
            } 
            else if (isset($_GET['addView']))
            {
                $this->model->addView();
                $books = $this->model->ListBook($_GET['id'],$_SESSION['curr']);          
                $selectedBook = $this->model->SelectedBook($_GET['id'],$_SESSION['curr']);
                include 'view/viewBook.php';  
            }   
         
            else if (isset($_GET['id'])){            
                $books = $this->model->ListBook($_GET['id'],$_SESSION['curr']);          
                $selectedBook = $this->model->SelectedBook($_GET['id'],$_SESSION['curr']);
                include 'view/viewBook.php';  
            }
         
            else if (isset($_GET['viewCart'])){

                include 'view/viewCart.php';  
            }  
         
            else if (isset($_GET['cat'])){
                     $category = $_GET['cat'];
                     $books = $this->model->CategoryBook($category);
                     include 'view/catalog.php'; 
            }
         
            else if (isset($_POST['LogIn']) or isset($_GET['profile']) or isset($_POST['Update']))
            {
                
                    $login = null;
                    if (isset($_POST['username'])){
                        $login = $this->model->userLogin($_POST['username'],$_POST['password']);
                    }

                    if(isset($_POST['Rusername']) && isset($_POST['Rpassword']) && isset($_POST['Remail']))
                    {
                        $this->model->userRegister();
                        include 'view/loginRes.php'; 
                    }
                    else if(isset($_GET['Update']) and isset($_GET['Update1']))
                    {
                        $info = $this->model->userInfo();
                        $order = $this->model->userOrder();
                        $viewlist = $this->model->userViewlist($_SESSION['curr']);
                        include 'view/loginUser.php'; 
                    }
                    else if($login == true or isset($_GET['profile']))
                    {
                        $info = $this->model->userInfo();
                        $order = $this->model->userOrder();
                        $viewlist = $this->model->userViewlist($_SESSION['curr']);
                        
                        include 'view/loginUser.php'; 
                    }
                    else if($login == false)
                    {
                        include 'view/loginRes.php';  
                        $message = "Username and/or Password incorrect.\\nTry again.";
                        echo "<script type='text/javascript'>alert('$message');</script>";             
                    }           
            }
            else if(isset($_GET['contact']))
            {
                    $this->model->contact(); 
                    include 'view/contact.php';           
            }
        
         
            else if(isset($_GET['logout']))
            {
                $_GET['shop'] = "set";
                $books = $this->model->ListBook($_GET['shop'],$_SESSION['curr']); 
                unset($_SESSION['login_user_ID']);
                include 'view/home.php';  
            }
            else if(isset($_GET['search']))
            {
                $books = $this->model->ListBook($_GET['search'],$_SESSION['curr']);          

                include 'view/search-form.php';  
            }
         
            else if(isset($_GET['book']))
            {
                $books = $this->model->ListBook($_GET['book'],$_SESSION['curr']);   
                $ID = $this->model->searchBook($_GET['book'],$_SESSION['curr']);
                if(isset($ID))
                {
                    $selectedBook = $this->model->SelectedBook($ID,$_SESSION['curr']); 
                    include 'view/viewBook.php';  
                }
                else
                {
                    include 'view/search-form.php';  
                }
            }          
            else if(isset($_GET['forgot']))
            {
                include 'view/forgotpass.php';  
            }
         
            else{
                include 'view/index.html';
            }   
         

     } 
    
    
    
}  
?>