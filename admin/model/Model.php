<?php
include("config.php");
include_once("controller/Controller.php");


    $file = $_FILES['Image_url'];
    $name = $file['name'];



class Model {

 public function getlogin($username,$password)
 {
    
    global $conn;
     
    $username = $_POST['username'];
    $password = $_POST['password'];
     
     

            $sql="SELECT * FROM admin";
            $res=mysqli_query($conn,$sql);

            if($res->num_rows > 0 )
                {
                    while($row = $res->fetch_assoc())
                    {
                        if ($row['username'] == $username and $row['password'] == $password)
                        {
                            return true;
                        }
                        
                    }
                }
            else 
                {	
                return false;
                }  
    
}

//Show BookList function
public function getBookList()  
    {   
        global $conn;
        
        $result = $conn->query("SELECT * from stocks");
        $return_result = array();
        
        if ($result->num_rows > 0) 
                {				
                    while($row = $result->fetch_assoc())
                    {  
    
                        $return_result[] = array( 
                            'ID' => $row['ID'],
                             'Title' => $row["Title"],
                             'Description' => $row["Description"],
                             'Price' => $row["Price"],
                             'Stock' => $row["Stocks"],
                             'Sold' => $row["Sold"],
                            'Author' => $row["Author"],
                            'Category' => $row["Category"],
                            'Image_url' => $row["Image_url"]
                            );
                    }    
                return $return_result;
                }
                else 
                {	
                return "No Record Found!<br><br>";
                }  
    }

//Get specific book function
public function getBook($title)
{
    global $conn;
        
        $result = $conn->query("SELECT * FROM stocks WHERE `ID` = '$title'");
        $return_result = array();
        
        if ($result->num_rows > 0) 
                {				
                    while($row = $result->fetch_assoc())
                    {  
                        //return var_dump($row);
                        $return_result[] = array( 
                            'ID' => $row['ID'],
                             'Title' => $row["Title"],
                             'Description' => $row["Description"],
                             'Price' => $row["Price"],
                             'Stock' => $row["Stocks"],
                             'Sold' => $row["Sold"],
                            'Author' => $row["Author"],
                            'Category' => $row["Category"],
                            'Image_url' => $row["Image_url"]
                            );
                    }    
                return $return_result;
                }
                else 
                {	
                    return "No Record Found!<br><br>";
                }
}

//Add book function
public function add($id, $title, $author, $description, $Price, $Category, $Stock)
{
    global $conn;

        global $name;
        

        //If input is not empty then run the query
        if(empty ($title))
	    {
            echo "No Entry.</br>";
	    }
        else
        {
            $Image_url = $name;
            $this->upload();
            
            $result = $conn->query("INSERT INTO stocks (Title, Author, Description, Price, Category, Stocks, Image_url) VALUE('$title','$author','$description', '$Price', '$Category','$Stock', '$name')");
            
            if ($result === TRUE) 
			{
				echo "<script type='text/javascript'>alert('New Record Added Successfully!')</script>";  
			} 
			else 
			{
				echo "</br></br>Error: " . $query . "<br>" . $conn->error;
            } 
        }
}
    


//Update books
public function update($id, $Image_url, $title, $author, $description, $Price, $Category, $Stock)
{
    global $conn;
  
    
    $check = $conn->query("SELECT ID FROM stocks WHERE ID  = '$id'");

    if($check != true)
	{
		echo "No Book Found.</br>";
	}
    else 
    {

        global $name;
        $Image_url = $name;
        $this->upload();
        
        $query = "";
        if (!empty($title))
        {
                $query .= " Title ='$title' ";
        }
        if (!empty($author))
        {
                $query .= " ,Author ='$author' ";
        }
        if (!empty($description))
        {
                $query .= " ,Description ='$description' ";
        } 
        if (!empty($Image_url))
        {
                $query .= " ,Image_url ='$Image_url' ";
        }
        if (!empty($Price))
        {
                $query .= " ,Price ='$Price' ";
        }
        if (!empty($Category))
        {
                $query .= " ,Category ='$Category' ";
        }
        if (!empty($Stock))
        {
                $query .= " ,Stocks ='$Stock' ";
        }
        
        $result = $conn->query("UPDATE stocks SET $query WHERE ID = '$id'");
        
        if ($result === TRUE) 
			{
				echo "<script type='text/javascript'>alert('Record Updated Successfully!')</script>";  
			} 
			else 
			{
				echo "</br></br>Error: " . $query . "<br>" . $conn->error;
            } 
    }
}

//Upload Picture Function
public function upload()
{
        global $file;
        global $name;
    
        if (!empty($name))
        {
            $path = "../images/books/" . basename($name);
            if (move_uploaded_file($file['tmp_name'], $path)) 
            {
                $thumbnail = $name;
            }
        }     
}

//Delete Function
public function delete($id)
{
        global $conn;
    
        $check = $conn->query("SELECT ID FROM stocks WHERE ID  = '$id'");

        if($check != true)
        {
            echo "No Book Found.</br>";
        }
        else 
        {

            $result = $conn->query("DELETE FROM stocks WHERE ID = '$id'");
        
            if ($result === TRUE) 
			{
				echo "<script type='text/javascript'>alert('Record Deleted Successfully!')</script>"; 
			} 
			else 
			{
				echo "</br></br>Error: " . $query . "<br>" . $conn->error;
            }  
        }       
}
    
}
?>
