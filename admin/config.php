<?php
//Connection
$servername = "localhost";
$username = "root";
$password = "";
$db = "bookstore";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $db);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
//echo "Connected successfully";

# set URL's constants for external data
define('RATES_URL',	'http://finance.yahoo.com/webservice/v1/symbols/allcurrencies/quote?format=xml');

# set path to local xml files
define('RATES',	'data/rates.xml');

# currency code array
$ccodes = array(
   'CAD','CNY','DKK',
   'EUR','GBP','HKD',
   'JPY','MXN','MYR',
   'NOK','NZD','PHP',
   'SEK','SGD','USD'); 
?>