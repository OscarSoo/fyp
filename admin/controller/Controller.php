<?php
include_once("model/Model.php");
session_start();
class Controller {
 public $model;
 
public function __construct()  
    {  
        $this->model = new Model();
    } 
 
public function invoke()
{

    
    if(!isset($_SESSION['Admin']))
    {
    switch (isset($_REQUEST['task']))
        {
            case "login":

                 if(isset($_REQUEST['submit']))
                {

                    if (empty($_REQUEST['username']) || empty($_REQUEST['password'])) 
                    {
                        echo  "Invalid Username or Password";
                    }
                    else
                    {
                        $result = $this->model->getlogin(($_REQUEST['username']),($_REQUEST['password']));
                        if($result == true)
                        {       
                                $_SESSION['Admin'] = 'SET';
                                $this->display();       
                        }
                        else
                        {
                            echo "Invalid user or password1";
                        }
                    }
                }

            break;

            default:
                include 'view/login.php';
            break;
        }

    }
    else if (isset($_SESSION['Admin']))
    {
        $this->logout();
        $this->delete();
        $this->display();     
       
    }
 }
  
public function display()  
     {   
    
          if (!isset($_GET['value']))  
          {  
               include 'view/homepage.php'; 
          } 
          else 
          {       
                switch ($_GET['value'])
                {
                    case 'add':
                    include 'view/add.php';
                    $this->add();
                    break;
                    
                    case 'view':
                    $books = $this->model->getBookList();
                    include 'view/view.php';
                    $this->update();  
                    break;
                    
                    default:                        
                    $books = $this->model->getBook($_GET['value']); 
                    include 'view/update.php'; 
                    $this->update();   
                    break;                      
                } 
          }  
     } 
    
public function logout()
    {
        if (isset($_GET['logout']))
          {            
            unset($_SESSION['Admin']);
            header ('Location: index.php');
          }
    }
 

public function add() 
{
    if (isset($_REQUEST['add']))
    {
        $this->model->add(($_REQUEST['ID']),($_REQUEST['Title']),($_REQUEST['Author']),($_REQUEST['Description']),($_REQUEST['Price']),
        ($_REQUEST['Category']),($_REQUEST['Stock']));
    }
} 
    
public function update()
{
    if (isset($_REQUEST['update']))
    {
        $this->model->update(($_REQUEST['ID']),($_REQUEST['Image_url']),($_REQUEST['Title']),($_REQUEST['Author']),($_REQUEST['Description']),($_REQUEST['Price']),
        ($_REQUEST['Category']),($_REQUEST['Stock']));
        header ('Location: index.php?value='.$_REQUEST['ID'].'');
    }           
}
    
public function delete() 
{   
    if (isset($_REQUEST['delete']))
    {   
        $this->model->delete(($_REQUEST['delete']));
        header ('Location: index.php?value=view');
    }
    
} 

    
}
    
?>