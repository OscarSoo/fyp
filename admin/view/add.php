<!DOCTYPE html>
<html lang="en">

<head>

    <title>BookStore Admin</title>

    <!-- Bootstrap Core CSS -->
    <link href="view/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="view/css/simple-sidebar.css" rel="stylesheet">


</head>

<body>

    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="../index.php">
                        书店 BookStore
                    </a>
                </li>
                <li>
                    <a href="index.php?dashboard">Dashboard</a>
                </li>
                <li>
                    <a href='index.php?value=add'>Add Book</a>
                </li>
                <li>
                    <a href="index.php?value=view">View Book</a>
                </li>

                <li>
                    <a href='index.php?logout=logout'>Logout</a>
                </li>
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <a href="#menu-toggle" class="btn btn-info btn-sm btn-warning" id="menu-toggle">Menu</a>
                      
                        
                        <h1>Add Book</h1><br><img src="view/banner/banner-bookstore.jpg" height="132" width="465">
                        

            <div class="table-responsive"> 
                <form method = "POST" enctype="multipart/form-data">
                    <table class="table" >
                    
                        <tbody>                       
                            <tr>                                                               
                                Select image to upload:<br><input type="file" name="Image_url">                         
                                <input type="hidden" name="ID" required><br><br>   
                                    
                                Title:<br><input type="text" name="Title" required size="35"><br><br>
                                    
                                Author:<br><input type="text" name="Author"  required size="35"><br><br>
                                    
                                Description:<br><textarea rows="10" cols="50" name="Description"  value="" required ></textarea><br><br>
                                    
                                Price:<br><input type="text" name="Price"  required size="35"><br><br>
                                    
                                Category:<br><input type="text" name="Category"  required size="35"><br><br>
                                                                
                                Stock:<br><input type="text" name="Stock" required size="35"><br><br>
                                    
                                <input class="btn btn-info btn-sm btn-warning" type="submit" name="add" value="Add"><br>
                                </td>
                            </tr>
                     </table>
                </form>
            </div>                    

                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="view/css/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="view/css/js/bootstrap.min.js"></script>
        
    <script src="view/css/js/update.js"></script>
        

    <!-- Menu Toggle Script -->
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>

</body>
</html>
